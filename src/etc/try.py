import collections
import sys

import ccxt





def collate_markets(markets):
    """
    Return a dictionary of markets indexed by 'BTC', 'ETH', and 'BNB'
    """
    _ = collections.defaultdict(list)
    for market in markets:
        _[market['quote']].append(market)

    return _

def main(exc):
    markets = exc.fetchMarkets()
    collated_markets = collate_markets(markets)
    # print(collated_markets)
    btc_markets = [m['symbol'] for m in collated_markets['BTC']]

    for btc_market in btc_markets:
        o = exc.fetchClosedOrders(btc_market)
        print(f"{btc_market}: {o}")


if __name__ == '__main__':

    exc = ccxt.binance({
        'apiKey': sys.argv[1],
        'secret': sys.argv[2]
    })

    print(main(exc))
